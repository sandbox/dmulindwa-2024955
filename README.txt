This modules provides a user interface to enable sending sms using the 
smsframework module. A block or page can be created, that will enable 
drupal send sms to many of the gateways supported by SmsFramework.
It can send SMS over several gateways.
